package com.devcamp.danh_sach_nhan_vien;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanhSachNhanVienApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanhSachNhanVienApplication.class, args);
	}

}
