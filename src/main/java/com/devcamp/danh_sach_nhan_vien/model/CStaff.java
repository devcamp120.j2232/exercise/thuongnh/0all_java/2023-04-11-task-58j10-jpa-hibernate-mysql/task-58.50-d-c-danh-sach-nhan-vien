package com.devcamp.danh_sach_nhan_vien.model;

import javax.persistence.*;


@Entity  // kết nối được với sql data base
@Table(name = "employee_list")  // ten cho data base
public class CStaff {

    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    private long id;

    @Column(name= "first_name")
    private String firstName;

    @Column(name= "last_name")
    private String lastName;

    @Column(name= "email")
    private String email;

    @Column(name= "age")
    private int age;

    @Column(name= "update_date")
    private long updateDate;

    public CStaff() {
    }

    public CStaff(long id, String firstName, String lastName, String email, int age, long updateDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.updateDate = updateDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "CStaff [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
                + ", age=" + age + ", updateDate=" + updateDate + "]";
    }

    

    

    

   



    


    
}
