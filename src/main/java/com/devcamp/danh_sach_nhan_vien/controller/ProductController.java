package com.devcamp.danh_sach_nhan_vien.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.danh_sach_nhan_vien.model.CStaff;
import com.devcamp.danh_sach_nhan_vien.repository.StaffRepository;

@RestController
@CrossOrigin
public class ProductController {

    @Autowired
    StaffRepository staffRepository;

    @GetMapping("/employeeList")
    public ResponseEntity<List<CStaff>> getProductsAll() {

        try {

            List<CStaff> products = new ArrayList<CStaff>();
            staffRepository.findAll().forEach(products::add);
            return new ResponseEntity<>(products, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
