package com.devcamp.danh_sach_nhan_vien.repository;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.devcamp.danh_sach_nhan_vien.model.CStaff;

public interface StaffRepository  extends JpaRepositoryImplementation<CStaff, Long> {
    
}
 